// Soal 1
console.log("Soal 1 : Arrow Function");
const luas = (panjang, lebar) => {
  const luas = panjang * lebar;
  return luas;
};
const keliling = (panjang, lebar) => {
  const keliling = 2 * (panjang + lebar);
  return keliling;
};
console.log("keliling : " + keliling(2, 4));
console.log("luas : " + luas(2, 4));
console.log();

// Soal 2
console.log("Soal 2 : Mengubah ke ES6 ");
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => console.log(`${firstName} ${lastName}`),
  };
};

//Driver Code
newFunction("William", "Imoh").fullName();
console.log();

// Soal 3
console.log("");
console.log("Soal 3 : Destructuring");

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);
console.log();

// Soal 4
console.log("Soal 4 : Array Spreading ES6");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
let combined = [...west, ...east];
//Driver Code
console.log(combined);
console.log();

// Soal 5
console.log("Soal 5 : Template Literal ES6");
const planet = "earth";
const view = "glass";
const theString = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`;
console.log(theString);
console.log();
