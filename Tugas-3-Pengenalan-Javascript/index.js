// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var a = pertama.substring(0, 4);
var b = pertama.substring(11, 19);
var c = kedua.substring(0, 7);
var d = kedua.substring(7, 18);
var e = d.toUpperCase();
var hasil = a.concat(b).concat(c).concat(e);
console.log("Soal 1 : Menggabungkan variabel");
console.log(hasil);

// Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
console.log("");
console.log("Soal 2 : Mengubah Tipe Data String ke Angka");
var angkaPertama = Number(kataPertama);
var angkaKedua = Number(kataKedua);
var angkaKetiga = Number(kataKetiga);
var angkaKeempat = Number(kataKeempat);
console.log(angkaPertama + angkaKedua * angkaKetiga + angkaKeempat);

// Soal 3
var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("");
console.log("Soal 3 : Selesaikan Variabel ");
console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);