// soal 1

// buatlah variabel seperti di bawah ini

// var nilai;
// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E
var nilai = 54;
var indeks = "";
if (nilai >= 85) {
  indeks = "A";
} else if (nilai >= 75 && nilai < 85) {
  indeks = "B";
} else if (nilai >= 65 && nilai < 75) {
  indeks = "C";
} else if (nilai >= 55 && nilai < 65) {
  indeks = "D";
} else if (nilai < 55) {
  indeks = "E";
}
console.log("");
console.log("Soal 1 : Pengkodisian If Else Elseif");
console.log("var Nilai : " + nilai);
console.log("Indeks : " + indeks);
console.log("");

// soal 2

// buatlah variabel seperti di bawah ini

var tanggal = 07;
var bulan = 11;
var tahun = 2000;
var sbulan;
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu
// muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing - masing)
switch (bulan) {
  case 1: {
    sbulan = " Januari ";
    break;
  }
  case 2: {
    ssbulan = " Februari ";
    break;
  }
  case 3: {
    sbulan = " Maret ";
    break;
  }
  case 4: {
    sbulan = " April ";
    break;
  }
  case 5: {
    sbulan = " Mei ";
    break;
  }
  case 6: {
    sbulan = " Juni ";
    break;
  }
  case 7: {
    sbulan = " Juli ";
    break;
  }
  case 8: {
    sbulan = " Agustus ";
    break;
  }
  case 9: {
    sbulan = " September ";
    break;
  }
  case 10: {
    sbulan = " Oktober ";
    break;
  }
  case 11: {
    sbulan = " November ";
    break;
  }
  case 12: {
    sbulan = " Desember ";
    break;
  }
  default: {
    sbulan = " Bulan Salah ";
  }
}
var stanggal = tanggal.toString();
var stahun = tahun.toString();

console.log("Soal 2 : Mengganti Bulan dengan Switch Case");
console.log("Bulan : " + bulan);
console.log(stanggal.concat(sbulan).concat(stahun));
console.log("");

// soal 3
console.log("Soal 3 : Looping");
// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).

// Output untuk n=7 :
var n = 7;
console.log("Output untuk n : " + n);
var bintang7 = "";
for (var n1 = n; n1 >= 1; n1--) {
  var n2 = 1;
  while (n2 >= 1) {
    bintang7 = bintang7 + "*";
    n2--;
  }
  console.log(bintang7);
}

console.log("");

// Soal 4
console.log("Soal 4 : Looping Nilai M");
console.log("Output untuk m = 7");
var m = 7;
for (var n = 1; n <= m; n++) {
  var b = n % 3;
  switch (b) {
    case 1: {
      console.log(n + " - I Love Programming");
      break;
    }
    case 2: {
      console.log(n + " - I Love Javascript");
      break;
    }
    default: {
      console.log(n + " - I Love Vue JS");
      var sd = "";
      for (var a = 0; a < n; a++) {
        sd = sd + "=";
      }
      console.log(sd);
      break;
    }
  }
}
console.log("");
