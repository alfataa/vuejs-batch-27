// Soal 1
console.log("Soal 1 : Mengurutkan Array & Menggunakan Loop");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// urutkan array di atas
var urut = daftarHewan.sort();
for (var a = 0; a < daftarHewan.length; a++) {
  // console.log(a);
  console.log(urut[a]);
}
console.log("");

// soal 2
console.log("Soal 2 : Menuliskan Function ");
// Tulislah sebuah function dengan nama introduce() yang
// memproses paramater yang dikirim menjadi sebuah kalimat
// perkenalan seperti berikut: "Nama saya [name], umur saya [age]
//  tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"
function introduce(data) {
  var kalimat;
  kalimat =
    "Nama saya " +
    data.name +
    ", Umur saya " +
    data.age +
    " tahun, alamat saya di " +
    data.address +
    ", dan saya punya hobby yaitu " +
    data.hobby;
  return kalimat;
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"
console.log("");
console.log("Soal 3 : Menghitung Huruf Vokal");

// soal 3
function isVowel(c) {
  // jika karakter terindeks dalam array [a,i,u,e,o]
  // return true
  // === -1 >> karakter tidak terindex dlm array
  return ["a", "e", "i", "o", "u"].indexOf(c.toLowerCase()) !== -1;
}
function hitung_huruf_vokal(data) {
  var teksArray = data.split("");
  // lakukan looping dengan mengecek vokal / tidaknya karakter yg menjadi nilai dlm array
  var jumlahVokal = 0;
  for (var n = 0; n < teksArray.length; n++) {
    // tambah nilai jumlah vokal jika hanya fungsi isVowel mereturn nilai true
    if (isVowel(teksArray[n]) === true) jumlahVokal++;
  }
  return jumlahVokal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2); // 3 2

console.log("");
console.log(
  "Soal 4 : Membuat Parameter Integer yang mengirim dan mengembalikan Nilai"
);
var angka = 0;
function hitung(angka) {
  return 2 * angka - 2;
}
console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
